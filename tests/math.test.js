const { sum, prod } = require('../src/math');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('prod 1 * 2 to equal 2', () => {
  expect(prod(1, 2)).toBe(2);
});